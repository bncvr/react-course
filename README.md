# Hello React

A JavaScript library for building user interfaces

## React Environment

### Emmet
- write in html 
- https://emmet.io


### Babel 
- js compiler
- convert ECMAScript 2015+ code into compatitble version of javascript

### JSX
- syntax extension of js
- remind template language
- produces react "elements"


## Components in React

### Stateless Functional Components
- state not managed 
- capitalized

### Why Class Components
- "this"
  - use ESX classes
- lifecycle methods

## Javascript Review

- collection of property/value pairs

### Object Literal Notation

- variables -> *properties*
- functions -> *methods*
- literal notation
  - ```var humanBeing = {hungry: true, age: 25, height: '5\'10'}```
- dot notation
  - better
  - ```car.wheels = 4 ```
- bracket notation
  - flexible for props with invalid variable names
  - ```car['wheels'] = 4 ```


### Props in React

- how components(*like js functions*) talk to each other
- passing data from parent to child
- flow downward
- immutable data

## Functional Programming

- pure functions
- pillar of javascript
- passing a function to a function as an argument
- higher order functions
  - filter, map, random
- pure functions
  - same input -> same output

## React Events

- similar to handling events on DOM elements
- syntatic differences:
  - React events named using camelCase *rather than lowercase*
  - with JSX pass a function as the event handler, *rather than a string*
- naming conventions
  - begin with *on* or *handle* -> ```onClick={handleButtonEdit}```
  - name the DOM element -> ```handleButtonEdit```
  - expected action -> ```Edit```
- ```this```
  - needed context
  - ```this.handleButtonEdit```

## This & Bind

- this is defined by creation context
- bind method
  - ES5
  - set the value of a function's ```this```
  - helps ```this``` find context
  - regardless of how it's called
  - **pass object as an argument** to bind method to find desired context 
- in objects ```this``` is set to the object method is called on


```
let cat = {} \\ creates object literal
cat = {
  sound: "meow",
  speak: function() {
    console.log(this.sound);
  }
};

cat.speack(); \\ "meow"

let speakFunction = cat.speak;
speakFunction(); \\ undefined 
\\ assigned value is function not method
\\ this has lost its context
```

*the interpreter sees*

```
let speakFunction = function() {
  console.log(this.sound);
}

speakFunction(); \\ undefined
```

*with bind*

```
let speakFunctionBind = speakFunction.bind(cat); \\ context of cat
speakFunctionBind(); \\ "meow"
```

*other examples*

```
let person1 = { name: "Alex" }
let person2 = { name: "Alexa" }

function namer() { return this.name; }

namer(); \\ ""
namer.bind(person1)(); \\ "Alex"
```

```
let number = { x: 24, y: 22 };
let count = function() {
  console.log(this.x + this.y);
}

count(); \\ NaN
count.bind(number)(); \\ 46
let boundFunc = count.bind(number);
boundFunc(); \\ 46
```

## State in React

- whenever an object is created via a class in js, js invokes the *constructor function*
- inside a constructor -> need to call *super()*
  - set a property
  - access "this"
  - ```this.onButtonClick = this.onButtonClick.bind(this);```
- fat arrow functions preserve this context when they are called



## Ajax Requests and the Fetch API

### Ajax 

- Asynchronous JavaScriptAndXML 
  - no need to refresh the page -> asynchrounous
  - receive and work with data from the server
- use of the XMLHttpRequest object
  - comunicate with servers

### API

- Application Program Interface
  - set of routines, protocols and tools for building software applications
  - specifies how software components should interact

### Syntax

```
componentDidMount() {
  fetch("api.com/people")
  .then(function(response) {
    return response.json()
  })
}

or

.then(response => response.json())
.then(responseData => {
  this.setState({
    items: responseData.results
  });
})
```

### JavaScript Fetch() method

- one argument: path to the resource you want to fetch
- returns a *promise* that resolves to the Response to that request
  - successfull or not

### JavaScript Promise object

- in a asynchronous operation (like ajax)
  - represents eventual completion or failure
  - resulting value

### Axios Library 

